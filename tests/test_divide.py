import pytest
from app.calculator import Calculator


def test_divide_success():
    assert Calculator.divide(1, 1) == 1

def test_divide_fail():
    assert Calculator.divide(1, 1) != 2
