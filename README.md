# Hello world!

[![pipeline status](https://gitlab.com/learning_badges/badge-python/badges/main/pipeline.svg)](https://gitlab.com/learning_badges/badge-python/-/commits/main) [![coverage report](https://gitlab.com/learning_badges/badge-python/badges/main/coverage.svg)](https://gitlab.com/learning_badges/badge-python/-/commits/main)


## How to run

```
python3 -m venv ENV
source ENV/bin/activate
pip install -r requirements.txt
python -m pytest --cov=. tests/
```
